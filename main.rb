#Exercicio 1
puts "Exercicio 1 \n"
def cast array
  i=0
  arrayAux=[]
  array.each do |element|
    arrayAux[i]="#{element}"
    i+=1
  end
return arrayAux
end

array = [25, 35, 45]
array=cast array
puts "#{array}"

#Exercicio 2
puts "\n\nExercicio 2 \n"
array=[[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
def operacoes array
  tam = array.length
  
  arraySoma=[]
  soma=[]
  arraySub=[]
  sub=[]
  arrayMult=[]
  mult=[]
  arrayDiv=[]
  div=[]

  for i in 0...tam
    soma[0]=array[i][0]
    sub[0]=array[i][0]
    mult[0]=array[i][0]
    div[0]=array[i][0]
    tam2 = array[i].length
    for j in 1...tam2
      soma[0]+=array[i][j]
      sub[0]-=array[i][j]
      mult[0]*=array[i][j]
      div[0]/=array[i][j].to_f
    end
    arraySoma[i]=soma.clone
    arraySub[i]=sub.clone
    arrayMult[i]=mult.clone
    arrayDiv[i]=div.clone
  end
  

  puts "Soma: #{arraySoma}"
  puts "Subtração: #{arraySub}"
  puts "Multiplicação: #{arrayMult}"
  puts "Divisão: #{arrayDiv}"
end

operacoes array

#Exercicio 3
puts "\n\nExercicio 3 \n"
hash={:chave1 => 5, :chave2 => 30, :chave3 => 20}
def quadrado hash
  array=[]
  i=0
  hash.each do |element|
    array[i]=(element[1]**2)
    i+=1
  end
  return array
end

array= quadrado hash

puts "#{array}"


#Exercicio 4
puts "\n\nExercicio 4 \n"
array=[3, 6, 7, 8]
def div3 array
  arrayAux=[]
  i=0
  
  array.each do |element|
    if element % 3 == 0
      arrayAux[i]=element
      i+=1
    end
  end
   
  return arrayAux
end

array=div3 array
puts "#{array}"

pausaVerResultados = gets.chomp